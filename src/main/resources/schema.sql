DROP TABLE IF EXISTS persona;

CREATE TABLE persona(
    id       BIGINT         PRIMARY KEY     IDENTITY,
    nombre   VARCHAR(50)    NOT NULL,
    edad     INT            NOT NULL
);

INSERT INTO persona (nombre, edad)
values
('Juan', 23),
('Pablo', 60),
('Colo', 25);