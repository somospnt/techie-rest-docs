package com.somospnt.restdocs.repository;

import com.somospnt.restdocs.domain.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Long> {
}
