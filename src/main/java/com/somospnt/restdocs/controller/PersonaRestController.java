package com.somospnt.restdocs.controller;

import com.somospnt.restdocs.domain.Persona;
import com.somospnt.restdocs.repository.PersonaRepository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("personas/")
public class PersonaRestController {

    private final PersonaRepository personaRepository;

    public PersonaRestController(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
    }

    @GetMapping("{id}")
    public Persona buscarPorId(@PathVariable Long id) {
        return personaRepository.findOne(id);
    }

    @PostMapping
    public Persona guardar(@RequestBody Persona persona) {
        return personaRepository.save(persona);
    }

    @DeleteMapping("{id}")
    public void borrar(@PathVariable Long id) {
        personaRepository.delete(id);
    }
}
