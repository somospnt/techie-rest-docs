package com.somospnt.restdocs.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.somospnt.restdocs.BaseTest;
import com.somospnt.restdocs.domain.Persona;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@AutoConfigureRestDocs("target/generated-snippets")
public class PersonaRestControllerTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RestDocumentationResultHandler restDocumentationResultHandler;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void buscarPorId_conId1_devuelvePersonaConId1() throws Exception {
        mockMvc
                .perform(get("/personas/1"))
                .andExpect(status().isOk())
                .andDo(restDocumentationResultHandler.document(
                        responseFields(
                                fieldWithPath("id").description("id de la persona"),
                                fieldWithPath("nombre").description("Nombre de la persona"),
                                fieldWithPath("edad").description("Edad de la persona")
                        )
                ));
    }

    @Test
    public void guardar_conPersonaValida_devuelvePersonaGuardada() throws Exception {
        Persona persona = new Persona();
        persona.setNombre("miller");
        persona.setEdad(50);
        String jsonPersona = objectMapper.writeValueAsString(persona);
        mockMvc
                .perform(
                        post("/personas/")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonPersona)
                )
                .andExpect(status().isOk())
                .andDo(restDocumentationResultHandler.document(
                        responseFields(
                                fieldWithPath("id").description("id de la persona"),
                                fieldWithPath("nombre").description("Nombre de la persona"),
                                fieldWithPath("edad").description("Edad de la persona")
                        )
                ));
    }

}
